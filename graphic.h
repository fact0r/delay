#define B_E         0x40
#define B_RST       0x20
#define B_CS2       0x10
#define B_CS1       8
#define B_SELECT    4
#define B_RW        2
#define B_RS        1

#define LCD_ON          0x3F
#define LCD_OFF         0x3E
#define LCD_SET_ADD     0x40
#define LCD_SET_PAGE    0xB8
#define LCD_DISP_START  0xC0
#define LCD_BUSY        0x80
