#include <stdint.h>

#define STK_CTRL  (* ((uint32_t *)  0xE000E010))
#define COUNTFLAG ((* ((uint8_t *) 0xE000E012)) & 1)
#define STK_LOAD  (* ((uint32_t *)  0xE000E014))
#define STK_VAL   (* ((uint32_t *)  0xE000E018))
#define STK_CALIB (* ((uint32_t *)  0xE000E01C))

void delay_250ns( void ) {
    STK_CTRL = 0;
    STK_LOAD = 42;
    STK_VAL = 0;
    STK_CTRL = 0b101;
    while (COUNTFLAG);
    STK_CTRL = 0;
}

void delay_mikro( unsigned int us) {
    for(int i = 0; i < us; i++) {
        delay_250ns();
        delay_250ns();
        delay_250ns();
        delay_250ns();
    }
}

void delay_milli( unsigned int ms) {
    #ifdef SIMULATOR
        ms = ms / 1000;
        ms++
    #endif
    for(int i = 0; i < ms; i++) {
        delay_mikro(1000);
    }
}