#include <stdint.h>

typedef struct {
    uint32_t    moder;
    uint16_t    otyper;
    uint16_t    otReserved;
    uint32_t    ospeedr;
    uint32_t    pupdr;
    uint8_t    idrLow;
    uint8_t    idrHigh;
    uint16_t    idrReserved;
    uint8_t    odrLow;
    uint8_t    odrHigh;
    uint16_t    odrReserved;
} GPIO;

#define GPIO_D (*((volatile GPIO*) 0x40020c00))
#define GPIO_E (*((volatile GPIO*) 0x40021000))
